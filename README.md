# README #

### What is this repository for? ###

* CIS2261 Final Project
* Rent-A-Car Vehicle Rental Agency

### How do I get set up? ###

* Go to rentacar.serverhosting.systems
* Admin Username: admin
* Admin Password: admin

### Who do I talk to? ###

* Brodie MacBeath
* Ben Bain
* Stéphane Levesque

* Any questions, comments, or concerns please email br1macbeath@gmail.com