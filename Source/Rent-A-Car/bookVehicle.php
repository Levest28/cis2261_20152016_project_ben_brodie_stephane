<?php
    /*
     * Author: Brodie MacBeath, Stéphane Levesque
     * Date: January 28, 2016
     * Updated: February 17, 2016
     * File: bookVehicle.php
     * Purpose: This page is used to book a specific vehicle depending on what vehicle the user selected to book
     */

    require_once 'includes/includesCore.php';

    //checks to make sure that the user did click submit to get to this page. If the user did not then redirect the user
    //to the vehicles.php page
    if(isset($_POST['submit'])){
        //Do nothing
    } else {
        header('Location: /vehicles.php');
    }

    //initializing variables
    $vin = $_POST['vin'];
    $make = "";
    $model = "";
    $imageLink = "";
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="assets/js/customJS.js"></script>
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/customCSS.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('includes/includesHeader.php');

            //connection to the database
            @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

            //if there is an error while connecting to the database then display the custom message that is below
            if (mysqli_connect_errno()) {
                echo "Error: Could not connect to database.  Please try again later.";
                exit;
            }

            //query for selecting the specific vehicle the user selected to book by taking the vin in through a hidden
            //variable
            $query = "SELECT * FROM vehicles WHERE vin = '$vin'";

            //variable to hold the result from the query
            $result = $database->query($query);

            //variable to hold the number of results that returned from that query
            $numberOfResults = $result->num_rows;

            if($numberOfResults > 0){
                ?>

                <div id='features-wrapper'>
                    <div class='container'>
                        <div class='row'>
                            <?php
                                while($row = $result->fetch_assoc()) {
                                    //variables to hold the vehicle information from the database
                                    $dailyCost = $row['dailyCost'];
                                    $make = $row['make'];
                                    $model = $row['model'];
                                    $imageLink = $row['imageLink'];
                                } ?>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo "<h2>There was an error booking that vehicle. Please try again.</h2>";
            }

            //close the database connection
            $database->close();
        ?>

        <div id="features-wrapper">
            <div class="container centered">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <section class="box feature">
                            <form id="frmBookVehicle" method="post">
                                <div class="inner">
                                    <header>
                                        <h2>Booking Information</h2><br/>
                                        <img src="<?php echo $imageLink;?>"/>
                                    </header>
                                    <header>
                                        <h2><?php echo $make . ' ' . $model;?></h2>
                                        <p>Daily Cost: <?php echo $dailyCost;?></p>
                                    </header>
                                    First Name: <input type="text" name="firstName"/>
                                    Last Name: <input type="text" name="lastName"/>
                                    Phone Number: <input type="text" name="phoneNumber"/>
                                    Email: <input type="email" name="email"/>
                                    License Number: <input type="text" name="licenseNumber"/>
                                    License Origin: <input type="text" name="licenseOrigin"/><br/>
                                    Rental Pick Up: <input type="date" name="rentalPickUp"/>
                                    Rental Drop Off: <input type="date" name="rentalDropOff"/>
                                </div>
                                <div class="centeredText">
                                    <input type="hidden" name="dailyCost" value="<?php echo $dailyCost;?>"/>
                                    <input type="hidden" name="vin" value="<?php echo $vin;?>"/>
                                    <span class="error" id="bookVehicleError"></span><br/>
                                    <input type="submit" id="btnBookVehicle" value="Book Vehicle"/>
                                </div>
                            </form>
                            <br/>
                        </section>
                    </div>
                </div>
            </div>
        </div>


        <!-- Footer -->
        <?php require("includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>

</body>
</html>


