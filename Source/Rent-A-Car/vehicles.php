<?php
    /*
     * Author: Brodie MacBeath, Stéphane Levesque
     * Date: January 28, 2016
     * Updated: February 17, 2016
     * File: vehicles.php
     * Purpose: This page shows the user all of the vehicles available at the current time. If the user selects a
     * specific vehicle type to choose or display the corresponding vehicle type will be displayed to the user.
     */

    require_once 'includes/includesCore.php';

    //initalizing variable for what type of vehicles to display to the user
    $selected = "";

    //checking to see if and what type of vehicle the user would like to see on the page
    if(isset($_GET['vehicles'])){
        if($_GET['vehicles'] == "trucks"){
            $selected = "Truck";
        } else if ($_GET['vehicles'] == "suvs"){
            $selected = "SUV";
        } else if ($_GET['vehicles'] == "cars"){
            $selected = "Car";
        }
    } else {
        $selected = "all";
    }
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/customCSS.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php
            require('includes/includesHeader.php');

            //connection to the database
            @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

            //if there is an error while connecting to the database then display the custom message that is below
            if (mysqli_connect_errno()) {
                echo "Error: Could not connect to database.  Please try again later.";
                exit;
            }

            //query for selecting all the vehicles in the database depending if the user clicked to view only
            //trucks, suvs, or cars, or if they wanted to see all of the vehicles we have
            if($selected == "all"){
                $query = "SELECT * FROM vehicles";
            } else {
                $query = "SELECT * FROM vehicles WHERE classification = '$selected' AND available = '1'";
            }

            //variable to hold the result from the query
            $result = $database->query($query);

            //variable to hold the number of results that returned from that query
            $numberOfResults = $result->num_rows;
            ?>

            <div id='features-wrapper'>
                <div class='container'>
                    <div class='row'>

            <?php
                //checks to make sure that there are vehicles that are able to be rented at the given time
                if($numberOfResults > 0){
                    while($row = $result->fetch_assoc()) {
                        //variables to hold the vehicle information from the database
                        $dailyCost = $row['dailyCost'];
                        $make = $row['make'];
                        $model = $row['model'];
                        $year = $row['year'];
                        $colour = $row['colour'];
                        $fuel = $row['fuel'];
                        $passengers = $row['passengers'];
                        $transmission = $row['transmission'];
                        $doors = $row['doors'];
                        $classification = $row['classification'];
                        $gps = $row['gps'];
                        $sunroof = $row['sunroof'];
                        $vin = $row['vin'];
                        $available = $row['available'];
                        $imageLink = $row['imageLink'];

                        //assigns specific variables for the gps to tell the user if there is a gps in the vehicle
                        if($gps == 1){
                            $gpsAvailable = "Yes";
                        } else {
                            $gpsAvailable = "No";
                        }

                        //assigns specific variables for the sunroof to tell the user if there is a sunroof in the vehicle
                        if($sunroof == 1){
                            $sunroofAvailable = "Yes";
                        } else {
                            $sunroofAvailable = "No";
                        }

                        //checks to make sure that the vehicle is available for renting at the time the user is booking
                        if($available == 1) {
                            //shows the information of the vehicle to the user
                            echo "<section class='box feature'>";
                                echo "<a class='image featured'><img src='$imageLink'/></a>";
                                echo "<form action='bookVehicle.php' method='post'>";
                                    echo "<div class='inner'>";
                                        echo "<header>";
                                            echo "<h2>$make $model</h2>";
                                        echo "</header>";
                                        echo "<p>Daily Cost: $$dailyCost</p>";
                                        echo "<p>Make: $make</p>";
                                        echo "<p>Model: $model</p>";
                                        echo "<p>Year: $year</p>";
                                        echo "<p>Colour: $colour</p>";
                                        echo "<p>Fuel: $fuel</p>";
                                        echo "<p>Passengers: $passengers</p>";
                                        echo "<p>Transmission: $transmission</p>";
                                        echo "<p>Number of doors: $doors</p>";
                                        echo "<p>GPS: $gpsAvailable</p>";
                                        echo "<p>Sunroof: $sunroofAvailable</p>";
                                    echo "</div>";
                                    echo "<input type='hidden' value='$vin' name='vin' id='vin'/>";
                                    echo "<div class='centeredText'>";
                                        echo "<input type='submit' name='submit' value='Book Vehicle'/>";
                                    echo "</div>";
                                echo "</form>";
                                echo "</br>";
                            echo "</section>";
                            echo "<div class='inner'></div>";
                        }
                    }
                } else {
                    echo "<section class='box feature'>";
                        echo "<div class='inner'>";
                            echo "<header>";
                                echo "<h2>Sorry there are no vehicles available at this time. Please try again later.</h2>";
                            echo "</header>";
                        echo "</div>";
                        echo "</br>";
                    echo "</section>";
            }?>
                            </div>
                        </div>
                    </div>
            <?php

                //close the database connection
                $database->close();

                //Footer
                require("includes/includesFooter.php");
            ?>
    </div>

    <!-- Scripts -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>

</body>
</html>

