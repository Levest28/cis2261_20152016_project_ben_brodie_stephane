<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: includesFooter.php
     * Purpose: This page is the footer for the different pages
     */
?>
<div id="footer-wrapper">
    <footer id="footer" class="container">
        <div class="row">

            <div class="3u 6u$(medium) 12u$(small)">
                <!-- Links -->
                <section class="widget links">
                    <h3>City of Charlottetown</h3>
                    <ul class="style2">
                        <li><a href="http://www.city.charlottetown.pe.ca/" target="_blank">The City of Charlottetown's Website</a></li>
                    </ul>
                </section>

            </div>
            <div class="3u 6u$(medium) 12u$(small)">

                <!-- Links -->
                <section class="widget links">
                    <h3>Province of P.E.I.</h3>
                    <ul class="style2">
                        <li><a href="http://www.gov.pe.ca/" target="_blank">The Province of Prince Edward Island's Website</a></li>
                    </ul>
                </section>

            </div>
            <div class="3u 6u(medium) 12u$(small)">

                <!-- Links -->
                <section class="widget links">
                    <h3>P.E.I. Tourism</h3>
                    <ul class="style2">
                        <li><a href="https://www.tourismpei.com/" target="_blank">Prince Edward Island's Tourism Website</a></li>
                    </ul>
                </section>

            </div>

            <div class="3u 6u$(medium) 12u$(small)">

                <!-- Contact -->
                <section class="widget contact last">
                    <h3>Contact Us</h3>
                    <ul>
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    </ul>
                    <p>Phone: 123-456-7890<br />
                        Fax: 098-765-4321<br />
                        Email: example@example.com</p>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="12u">
                <div id="copyright">
                    <ul class="menu">
                        <li>&copy; Rent-A-Car. All rights reserved</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
