<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: includesHeader.php
     * Purpose: This page is the header for the different pages
     */
?>
<!-- Header -->
<div id="header-wrapper">
    <header id="header" class="container">

        <!-- Logo -->
        <div id="logo">
            <h1><a href="/index.php">Rent-A-Car</a></h1>
        </div>

        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li <?php if ($_SERVER['REQUEST_URI'] == 'index.php') { echo "class='current'";} ?>><a href="/index.php">Home</a></li>
                <li <?php if ($_SERVER['REQUEST_URI'] == 'vehicles.php') { echo "class='current'";} ?>>
                    <a href="/vehicles.php">Vehicles</a>
                    <ul>
                        <li><a href="/vehicles.php?vehicles=trucks">Pick-Up Trucks</a></li>
                        <li><a href="/vehicles.php?vehicles=suvs">SUVs</a></li>
                        <li><a href="/vehicles.php?vehicles=cars">Cars</a></li>
                    </ul>
                </li>
                <li <?php if ($_SERVER['REQUEST_URI'] == 'about.php') { echo "class='current'";} ?>><a href="/about.php">About</a></li>
                <li <?php if ($_SERVER['REQUEST_URI'] == 'help.php') { echo "class='current'";} ?>><a href="/help.php" class="icon fa-question-circle">Help</a></li>
                <?php
                    if(isset($_SESSION['adminLoggedIn'])){
                        if($_SESSION['adminLoggedIn']){
                            ?>
                            <li <?php if ($_SERVER['REQUEST_URI'] == 'adminReturnVehicle.php' || $_SERVER['REQUEST_URI'] == 'adminDailyRentalReport.php' || $_SERVER['REQUEST_URI'] == 'adminMonthlyRentalReport.php') { echo "class='current'";} ?>>
                                <a href=''>Admin Tools</a>
                                <ul>
                                    <li><a href='/admin/adminReturnVehicle.php'>Return Vehicle</a></li>
                                    <li><a href='/admin/adminRentVehicle.php'>Rent Vehicle</a></li>
                                    <li><a href='/admin/adminDailyRentalReport.php'>Daily Rental Report</a></li>
                                    <li><a href='/admin/adminMonthlyRentalReport.php'>Monthly Rental Report</a></li>
                                    <li><a href='/admin/adminLogout.php?action=restart'>Admin Logout</a></li>
                                </ul>
                            </li>
                        <?php
                        } else {
                            ?>
                            <li <?php if ($_SERVER['REQUEST_URI'] == 'adminLogin.php') { echo "class='current'";} ?>><a href='/admin/adminLogin.php'>Admin Login</a></li>
                        <?php
                        }
                    } else {
                        ?>
                        <li <?php if ($_SERVER['REQUEST_URI'] == 'adminLogin.php') { echo "class='current'";} ?>><a href='/admin/adminLogin.php'>Admin Login</a></li>
                    <?php
                    }
                ?>
            </ul>
        </nav>
    </header>
</div>