<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: includesCheckAuthorization.php
     * Purpose: This page is the check authorization page that checks to makes sure that the administrator is logged in
     * to make sure that only administrators can access certain pages.
     */

    //Checking to see if there is a user logged in and if there is not then redirect them to the main index.php page
    if(isset($_SESSION['adminLoggedIn'])){
        if($_SESSION['adminLoggedIn'] == false){
            header('Location: index.php');
        } else if($_SESSION['adminLoggedIn'] == true) {
            //Do nothing
        } else {
            header('Location: index.php');
        }
    } else {
        header('Location: index.php');
    }