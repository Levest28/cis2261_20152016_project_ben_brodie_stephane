<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: adminLogin.php
     * Purpose: This page is the admin login page for allowing the administrator to login to see some of the admin
     * features that the normal user cannot access.
     */

    //allowing access to the information for the database
    require_once '../includes/includesCore.php';
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="../assets/js/customJS.js"></script>
    <link rel="stylesheet" href="../assets/css/main.css" />
    <link rel="stylesheet" href="../assets/css/customCSS.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../assets/css/ie8.css"/><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('../includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <h2 class="centeredText">Administration Login</h2>
                        <p class="centeredText">Please enter your administration credentials into the areas provided.</p><br/>
                        <form id="frmAdminLogin" method="POST">
                            <input type="text" name="username" id="username" placeholder="Username" style="display: inline; width: 49%;"/>
                            <input type="password" name="password" id="password" placeholder="Password" style="display: inline; width: 49%;"/><br/><br/>
                            <div class="centeredText">
                                <span class="error" id="adminLoginError"></span><br/>
                                <input type="submit" value="Login" name="login" id="btnLoginSubmit" style="width: 30%;"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require("../includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/jquery.dropotron.min.js"></script>
    <script src="../assets/js/skel.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="../assets/js/main.js"></script>

</body>
</html>