<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: adminLogout.php
     * Purpose: This is the admin logout page that destroys the session and gets rid of all the cookies for this webpage
     */

    //allowing access to the information for the database
    require_once '../includes/includesCore.php';

    //Checks to see if the user clicked the logout button and if they did then log the user out
    if(isset($_GET['action'])){
        //Destroys the session
        session_destroy();

        // this part gets rid of the session cookie that php puts on the users machine
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        //header redirect to index.php page
        header('Location: /index.php');
    }