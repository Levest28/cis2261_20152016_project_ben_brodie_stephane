<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: adminReturnVehicle.php
     * Purpose: This is the admin return vehicle page that allows the administrator to return the vehicle and switch it
     * in the system so that it is available for the customers again.
     */

    //allowing access to the information for the database
    require_once('../includes/includesCore.php');
    //checking to make sure that the admin is logged in
    require('../includes/includesCheckAuthorization.php');

    //Initializing variables
    $model = "";
    $make = "";
    $availableToPublic = "";
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="../assets/js/customJS.js"></script>
    <link rel="stylesheet" href="../assets/css/main.css" />
    <link rel="stylesheet" href="../assets/css/customCSS.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../assets/css/ie8.css"/><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('../includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <h2 class="centeredText">Administration Return Vehicle</h2>
                        <p class="centeredText">Please enter the VIN of the vehicle that has been returned.</p><br/>
                        <form id="frmAdminVehicleReturn" method="POST">
                            <div class="centeredText">
                            <input type="text" name="vin" id="vin" placeholder="Vehicle Identification Number"/>
                                <span class="error" id="adminReturnVehicleError"></span><br/>
                                <input type="submit" value="Return Vehicle" name="returnVehicle" id="btnReturnVehicle" style="width: 30%;"/><br/><br/>
                            </div>
                        </form>
                        <?php
                            //connection to the database
                            @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

                            //if there is an error while connecting to the database then display the custom message that is below
                            if (mysqli_connect_errno()) {
                                echo "Error: Could not connect to database.  Please try again later.";
                                exit;
                            }

                            //getting the current day, month, and year
                            $currentDay = date('d');
                            $currentMonth = date('m');
                            $currentYear = date('Y');

                            //query for selecting all of the rentals that are being returned today
                            $query = "SELECT * FROM rentals WHERE rentalEndDate = '$currentYear-$currentMonth-$currentDay'";

                            //variable to hold the result from the query
                            $result = $database->query($query);

                            //variable to hold the number of results that returned from that query
                            $numberOfResults = $result->num_rows;

                            //checking to make sure there are vehicles being returned today. If not show
                            //an error message to the admin
                            if($numberOfResults > 0) {
                                echo "<table class='centeredText' style='border: 2px solid black;'>";
                                echo "<tr><th>VIN</th><th>Today's Date</th><th>First Name</th><th>Last Name</th><th>Vehicle</th><th>Total Cost</th><th>Rental Start Date</th><th>Rental End Date</th><th>Driver's License Number</th></tr>";
                                while($row = $result->fetch_assoc()){
                                    //variables to hold the vehicle information in for displaying to the administrator
                                    $todaysDate = $currentYear . "-" . $currentMonth . "-" . $currentDay;
                                    $firstName = $row['nameFirst'];
                                    $lastName = $row['nameLast'];
                                    $totalCost = $row['totalCost'];
                                    $rentalStartDate = $row['rentalStartDate'];
                                    $rentalEndDate = $row['rentalEndDate'];
                                    $driversLicenseNumber = $row['licenseNumber'];

                                    $vin = $row['vin'];

                                    //query for getting the vehicle information
                                    $vehicleInfoQuery = "SELECT * FROM vehicles WHERE vin = '$vin' LIMIT 1";

                                    //variable to hold the result from the query
                                    $result = $database->query($vehicleInfoQuery);

                                    //looping through the different vehicles to get the make and model and check to see
                                    //if they have been returned already
                                    while($row = $result->fetch_assoc()) {
                                        $model = $row['model'];
                                        $make = $row['make'];

                                        if($available = $row['available'] == "1"){
                                            $availableToPublic = "True";
                                        } else {
                                            $availableToPublic = "False";
                                        }
                                    }

                                    //assigning a variable to hold the make and model for displaying
                                    $vehicle = $make . " " . $model;

                                    //checking to make sure that the vehicle has not been returned yet and if it has not
                                    //then display to the user
                                    if($availableToPublic == "False"){
                                        echo "<tr><td>$vin</td><td>$todaysDate</td><td>$firstName</td><td>$lastName</td><td>$vehicle</td><td>$totalCost</td><td>$rentalStartDate</td><td>$rentalEndDate</td><td>$driversLicenseNumber</td></tr>";
                                    } else {
                                        //Do nothing
                                    }
                                }
                                echo "</table>";
                            } else {
                                //Displaying a message to the user if there are no comments in the database
                                echo "<h3 class='centeredText'>There is no vehicles being returned today.</h3>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require("../includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/jquery.dropotron.min.js"></script>
    <script src="../assets/js/skel.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="../assets/js/main.js"></script>

</body>
</html>