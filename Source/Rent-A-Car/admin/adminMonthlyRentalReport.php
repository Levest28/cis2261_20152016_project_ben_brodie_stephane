<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: adminMonthlyRentalReport.php
     * Purpose: This is the admin monthly rental report page that shows the rentals according to the current month
     */

    //allowing access to the information for the database
    require_once('../includes/includesCore.php');
    //checking to make sure that the admin is logged in
    require('../includes/includesCheckAuthorization.php');

    //Initializing variables
    $timesRented = 0;
    $monthlyIncome = 0;
    $model = "";
    $make = "";
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="../assets/js/customJS.js"></script>
    <link rel="stylesheet" href="../assets/css/main.css" />
    <link rel="stylesheet" href="../assets/css/customCSS.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="../assets/css/ie8.css"/><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('../includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <h2 class="centeredText">Administration Monthly Rental Report</h2><br/>
                        <?php
                            //connection to the database
                            @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

                            //if there is an error while connecting to the database then display the custom message that is below
                            if (mysqli_connect_errno()) {
                                echo "Error: Could not connect to database.  Please try again later.";
                                exit;
                            }

                            //getting the current day, month, and year
                            $currentMonth = date('M');
                            $currentYear = date('Y');

                            //query for selecting all of the rentals that are being rented this month or returned this month
                            $query = "SELECT * FROM rentals WHERE rentalStartDate = '' OR rentalEndDate = ''";

                            //variable to hold the result from the query
                            $result = $database->query($query);

                            //variable to hold the number of results that returned from that query
                            $numberOfResults = $result->num_rows;

                            //checking to make sure there are vehicles being rented or returned today. If not show
                            //an error message to the admin
                            if($numberOfResults > 0) {
                                echo "<table class='centeredText' style='border: 2px solid black;'>";
                                echo "<tr><th>$currentMonth-$currentYear</th><th>Times Rented</th><th>Monthly Income</th></tr>";
                                while($row = $result->fetch_assoc()){
                                    //variables to hold the vehicle information in for displaying to the administrator
                                    $totalCost = $row['totalCost'];
                                    $vin = $row['vin'];

                                    //query for getting the vehicle information
                                    $vehicleInfoQuery = "SELECT * FROM vehicles WHERE vin = '$vin' LIMIT 1";

                                    //variable to hold the result from the query
                                    $result = $database->query($vehicleInfoQuery);

                                    //looping through the different vehicles to get the make and model
                                    while($row = $result->fetch_assoc()) {
                                        $model = $row['model'];
                                        $make = $row['make'];
                                    }

                                    //assigning a variable to hold the make and model for displaying
                                    $vehicle = $make . " " . $model;

                                    //Displaying the vehicle information to the administrator
                                    echo "<tr><td></td><td>$timesRented</td><td>$monthlyIncome</td></tr>";
                                }
                                echo "</table>";
                            } else {
                                //Displaying a message to the user if there are no comments in the database
                                echo "<h3 class='centeredText'>There is no monthly activity for the current month.</h3>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require("../includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/jquery.dropotron.min.js"></script>
    <script src="../assets/js/skel.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <!--[if lte IE 8]>
    <script src="../assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="../assets/js/main.js"></script>

</body>
</html>