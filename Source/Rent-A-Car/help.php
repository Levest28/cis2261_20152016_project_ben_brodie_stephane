<?php
    /*
     * Author: Brodie MacBeath, Ben Bain, Stéphane Levesque
     * Date: January 27, 2016
     * Updated February 17, 2016
     * File: help.php
     * Purpose: This page is for the user to be able to fill out a form and send an email to the company
     */

    require_once 'includes/includesCore.php';

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car - Help</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="assets/js/customJS.js"></script>
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/customCSS.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <h2>Contact Us</h2>
                        <p>Please use the text fields below to contact us with any questions or concerns.</p><br/>
                        <form id="contactUsForm" method="post">
                            <input type="text" name="firstName" id="firstName" placeholder="First Name" style="display: inline; width: 49%;"/>
                            <input type="text" name="lastName" id="lastName" placeholder="Last Name" style="display: inline; width: 49%;"/><br/><br/>
                            <input type="text" name="email" id="email" placeholder="Email" style="display: inline; width: 49%;"/>
                            <input type="text" name="phone" id="phone" placeholder="Phone Number" style="display: inline; width: 49%;"/><br/><br/>
                            <textarea name="message" id="message" placeholder="Message"></textarea><br/>
                            <div class="centeredText">
                                <span class="error" id="contactUsError"></span><br/>
                                <input type="submit" id="btnContactSubmit" value="Send Email" style="width: 30%;"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require("includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>

</body>
</html>