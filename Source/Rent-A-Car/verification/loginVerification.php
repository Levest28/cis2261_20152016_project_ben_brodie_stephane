<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: loginVerification.php
     * Purpose: This page is the login verification page that verify's the admin login
     */

    require_once('../includes/includesCore.php');

    //connection to the database
    @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

    //if there is an error while connecting to the database then display the custom message that is below
    if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.";
        exit;
    }

    //Sanitizing the information the user entered in the text boxes
    $username = $database->real_escape_string(trim($_POST['username']));
    $password = $database->real_escape_string(trim($_POST['password']));

    //query for checking to make sure that the username and password entered matched what is in the database
    $query = "SELECT COUNT(*) FROM users WHERE username = '" . $username . "' AND password = md5('" . $password . "') LIMIT 1";

    //variable to hold the result from the query
    $result = $database->query($query);

    //variable to hold the amount of rows that were returned from running that query
    $row = $result->fetch_row();

    //checks to see if the query was successful and if so then set the session variable to say the admin is logged in
    if ($row[0] == 1) {
        $_SESSION['adminLoggedIn'] = true;
        echo "true";
    } else {
        echo "false";
    }

    //close the database connection
    $database->close();