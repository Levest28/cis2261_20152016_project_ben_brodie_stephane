<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: bookVehicleVerification.php
     * Purpose: This page is the book vehicle verification page that checks to make sure the user entered all of the
     * required information and make sure that it is valid as well. If it is not then it sends the user and error message
     * letting them know what they need to change. It then inserts the information entered to the database
     */

    require_once '../includes/includesCore.php';

    //connection to the database
    @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

    //if there is an error while connecting to the database then display the custom message that is below
    if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.";
        exit;
    }

    //Sanitizing the information the user entered in the text boxes
    $vin = $database->real_escape_string($_POST['vin']);
    $firstName = $database->real_escape_string(trim($_POST['firstName']));
    $lastName = $database->real_escape_string(trim($_POST['lastName']));
    $phoneNumber = $database->real_escape_string(trim($_POST['phoneNumber']));
    $email = $database->real_escape_string(trim($_POST['email']));
    $licenseNumber = $database->real_escape_string(trim($_POST['licenseNumber']));
    $licenseOrigin = $database->real_escape_string(trim($_POST['licenseOrigin']));
    $rentalPickUp = $database->real_escape_string(trim($_POST['rentalPickUp']));
    $rentalDropOff = $database->real_escape_string(trim($_POST['rentalDropOff']));
    $dailyCost = $database->real_escape_string($_POST['dailyCost']);

    //Validation to make sure that all of the information that the user provided is filled in and valid.
    if($firstName == ""){
        echo "noFirstName";

        //close the database connection
        $database->close();

        exit;
    } else if ($lastName == ""){
        echo "noLastName";

        //close the database connection
        $database->close();

        exit;
    } else if ($phoneNumber == ""){
        echo "noPhoneNumber";

        //close the database connection
        $database->close();

        exit;
    } else if (strlen($phoneNumber) != 10) {
        echo "phoneNumberNot10Numbers";

        //close the database connection
        $database->close();

        exit;
    }  else if (!is_numeric($phoneNumber)) {
        echo "phoneNumberNotNumeric";

        //close the database connection
        $database->close();

        exit;
    } else if ($email == ""){
        echo "noEmail";

        //close the database connection
        $database->close();

        exit;
    } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "emailNotValid";

        //close the database connection
        $database->close();

        exit;
    } else if ($licenseNumber == ""){
        echo "noLicenseNumber";

        //close the database connection
        $database->close();

        exit;
    } else if ($licenseOrigin == ""){
        echo "noLicenseOrigin";

        //close the database connection
        $database->close();

        exit;
    } else if ($rentalPickUp == ""){
        echo "noRentalPickUp";

        //close the database connection
        $database->close();

        exit;
    } else if ($rentalDropOff == ""){
        echo "noRentalDropOff";

        //close the database connection
        $database->close();

        exit;
    } else if(strtotime($rentalPickUp) < strtotime("-1 day")){
        echo "pickUpDateInThePast";

        //close the database connection
        $database->close();

        exit;
    } else if(strtotime($rentalDropOff) < strtotime("-1 day")){
        echo "dropOffDateInThePast";

        //close the database connection
        $database->close();

        exit;
    } else if(strtotime($rentalDropOff) < strtotime($rentalPickUp)){
        echo "dateInvalid";

        //close the database connection
        $database->close();

        exit;
    } else if(strtotime($rentalDropOff) == strtotime($rentalPickUp)){
        echo "dateTheSame";

        //close the database connection
        $database->close();

        exit;
    }

    //query for inserting a new comment into the database
    $customerInsertQuery = "INSERT INTO rentals (`vin`, `rentalStartDate`, `rentalEndDate`, `nameFirst`, `nameLast`, `phone`, `email`, `licenseNumber`, `licenseOrigin`) VALUES ('$vin', '$rentalPickUp', '$rentalDropOff', '$firstName', '$lastName', '$phoneNumber', '$email', '$licenseNumber', '$licenseOrigin');";

    //variable to hold the result from the query
    $customerInsertQueryResult = $database->query($customerInsertQuery);

    //checks to see if the query was successful and if so then redirect the user to the main index.php page
    //and if it is not successful then display an error message telling the user it was unsuccessful
    if($customerInsertQueryResult){

        $maxId = "SELECT MAX(id) FROM rentals";

        $maxIdResult = $database->query($maxId);

        $maxIdRow = $maxIdResult->fetch_row();

        $rentalLengthQuery = "SELECT DATEDIFF((SELECT `rentalEndDate` FROM `rentals` WHERE `id` = '$maxIdRow[0]'), (SELECT `rentalStartDate` FROM `rentals` WHERE `id` = '$maxIdRow[0]')) AS days";

        $rentalLengthResult = $database->query($rentalLengthQuery);

        //variable to hold the amount of rows that were returned from running that query
        $row = $rentalLengthResult->fetch_row();

        $totalCost = $dailyCost * $row['0'];

        $updateTotalCostQuery = "UPDATE rentals SET totalCost = '$totalCost' WHERE id = '$maxIdRow[0]'";

        $updateTotalCostResult = $database->query($updateTotalCostQuery);
        echo "true";
    } else {
        echo "false";
    }

    //close the database connection
    $database->close();