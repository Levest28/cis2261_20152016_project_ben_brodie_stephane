<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: returnVehicleVerification.php
     * Purpose: This page is the rent vehicle verification page that verify's to make sure that the admin entered
     * all of the information required. This page is used when a vehicle is rented to put it in the system as
     * unavailable to the public to book
     */

    require_once '../includes/includesCore.php';

    //connection to the database
    @ $database = new mysqli($databaseLocation, $databaseUsername, $databasePassword, $databaseName);

    //if there is an error while connecting to the database then display the custom message that is below
    if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.";
        exit;
    }

    //Sanitizing the information the user entered in the text boxes
    $vin = $database->real_escape_string(trim($_POST['vin']));

    //query for selecting the number of number that have that vin
    $selectQuery = "SELECT COUNT(*) FROM vehicles WHERE vin = '$vin'";

    //variable to hold the result from the query
    $selectQueryResult = $database->query($selectQuery);

    //variable to hold the amount of rows that were returned from running that query
    $row = $selectQueryResult->fetch_row();

    //query for updating and setting the availability for the specific vehicle
    $query = "UPDATE vehicles SET available = '0' WHERE vin = '$vin'";

    //variable to hold the result from the query
    $result = $database->query($query);

    //checks to see if the query was successful and if so then it echos true which sends back to the page with a successful
    //message, if it is not successful then send back different error messages to the user to let them know if there
    //was no vin entered or if there was no vehicle in the database with that vin
    if($vin == ""){
        echo "noVIN";
    } else if($row[0] == 0){
        echo "noVehicle";
    } else if ($result) {
        echo "true";
    } else {
        echo "false";
    }

    //close the database connection
    $database->close();