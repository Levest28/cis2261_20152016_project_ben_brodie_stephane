<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 27, 2016
     * Updated: February 17, 2016
     * File: contactUsVerification.php
     * Purpose: This page is the contact us verification page that checks to make sure all of the information entered
     * by the user is correct and valid if it is not then it sends an error message back to the user.
     */

    require_once '../includes/includesCore.php';

    //make sure that all of the information that is entered is trimmed
    $firstName = trim($_POST['firstName']);
    $lastName = trim($_POST['lastName']);
    $email_address = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $message = trim($_POST['message']);

    // Create the email and send the message
    $to = 'br1macbeath@gmail.com';
    $email_subject = "Website Contact Form:  $firstName $lastName";
    $email_body = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $firstName $lastName\n\nEmail: $email_address\n\nPhone: $phone\n\nMessage:\n$message";
    $headers = "From: noreply@rentacar.com\n";

    //Check for empty fields
    if(empty($firstName)){
        echo "noFirstName";
    } else if (empty($lastName)){
        echo "noLastName";
    } else if (empty($email_address)){
        echo "noEmailAddress";
    } else if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        echo "noValidEmail";
    } else if (empty($phone)){
        echo "noPhoneNumber";
    } else if (empty($message)){
        echo "noMessage";
    } else if(mail($to,$email_subject,$email_body,$headers)){
        echo "true";
    } else {
        echo "false";
    }