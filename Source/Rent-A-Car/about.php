<?php
    /*
     * Author: Brodie MacBeath
     * Date: January 28, 2016
     * Updated: February 17, 2016
     * File: about.php
     * Purpose: This page is an information page about the company but more specifically the employees
     */

    require_once 'includes/includesCore.php';


?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car - Help</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/customCSS.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="12u 12u(medium)">
                        <h2>About Rent-A-Car!</h2>
                        <p>Rent-A-Car is a locally owned car rental agency that exceeds in customer service and value our products.</p><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="12u 12u(medium)">
                        <!-- Information on ben bain -->
                        <h3>Ben Bain</h3>
                        My name is Ben Bain, I am currently a student at Holland College taking the Computer Information Systems (CIS) course.
                        I previously have attended Grand Tracadie Elementary through grades 1 to 6, Stonepark Intermediate School through grade 7 to 9, and Charlottetown Rural High School through grade 10 to 12.
                        I graduated with highest aggregate in Computer 801 at Charlottetown Rural High School and always had a keen interest for business.
                        In the CIS course I have gained knowledge in many development languages including Java, PHP/HTML/JavaScript, C#, along with MySql database.
                        After I graduate the CIS course I hope to pursue a job with the Department of Veterans Affairs.
                        <br/><br/>
                        <section class="widget contact last">
                            <ul>
                                <li><a href="https://twitter.com/Bainer_08" class="icon fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
                                <li><a href="https://www.facebook.com/bbain3" class="icon fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
                            </ul><br/>
                        </section>
                    </div>

                    <div class="12u 12u(medium)">
                        <!-- Information on brodie macbeath -->
                        <h3>Brodie MacBeath</h3>
                        My name is Brodie MacBeath, I am currently a student at Holland College taking the Computer Information Systems (CIS) course.
                        I previously have attended L.M. Montgomery School through grades 1 to 6, Stonepark Intermediate School through grade 7 to 9, and Charlottetown Rural High School through grade 10 to 12.
                        I graduated with honors every year throughout Charlottetown Rural High School.
                        In the CIS course I have taken Introduction to Object Oriented Programming, Operating Systems, Networking, Business Communications and my favorite course, Web Application Development.
                        After I graduate the CIS course I hope to pursue a job in the Web Application industry.
                        <br/><br/>
                        <section class="widget contact last">
                            <ul>
                                <li><a href="https://twitter.com/brodiemacb" class="icon fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
                                <li><a href="https://www.facebook.com/brodie.macbeath" class="icon fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
                            </ul><br/>
                        </section>
                    </div>

                    <div class="12u 12u(medium)">
                        <!-- Information on stéphane levesque -->
                        <h3>Stéphane Levesque</h3>
                        My name is Stéphane Levesque, I am currently a student at Holland College taking the Computer Information Systems (CIS) course.
                        I previously have attended Divine Infant Elementary School in Ottawa through grades 1 to 5, Henry Larsen Elementary School in Ottawa through grade 6 to 8, Cairine Wilson Secondary School through grade 9 to 11, and Charlottetown Rural High School for grade 12.
                        I graduated with Honours: Average of 85.625%, Highest in History 631, and Highest in Mathematics 631.
                        In the CIS course I have taken Introduction to Object Oriented Programming, Operating Systems, Networking, Business Communications and my favorite course, Web Application Development.
                        After I graduate the CIS course I hope to pursue a job in the Web Application industry.
                        <br/><br/>
                        <section class="widget contact last">
                            <ul>
                                <li><a href="#" class="icon fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
                                <li><a href="https://www.facebook.com/stephane.levesque.543" class="icon fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require("includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>

</body>
</html>