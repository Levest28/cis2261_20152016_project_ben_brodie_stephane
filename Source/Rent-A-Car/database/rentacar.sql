-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2016 at 02:19 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rentacar`
--
CREATE DATABASE IF NOT EXISTS `rentacar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `rentacar`;

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

CREATE TABLE IF NOT EXISTS `rentals` (
  `id` int(11) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `rentalStartDate` date NOT NULL,
  `rentalEndDate` date NOT NULL,
  `totalCost` int(11) NOT NULL,
  `nameFirst` varchar(20) NOT NULL,
  `nameLast` varchar(20) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `licenseNumber` varchar(30) NOT NULL,
  `licenseOrigin` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
  (1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL,
  `dailyCost` int(11) NOT NULL,
  `make` varchar(20) NOT NULL,
  `model` varchar(30) NOT NULL,
  `year` year(4) NOT NULL,
  `colour` varchar(30) NOT NULL,
  `fuel` varchar(10) NOT NULL,
  `passengers` int(11) NOT NULL,
  `transmission` varchar(10) NOT NULL,
  `doors` int(11) NOT NULL,
  `classification` varchar(20) NOT NULL,
  `gps` tinyint(1) NOT NULL,
  `sunroof` tinyint(1) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  `imageLink` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `dailyCost`, `make`, `model`, `year`, `colour`, `fuel`, `passengers`, `transmission`, `doors`, `classification`, `gps`, `sunroof`, `vin`, `available`, `imageLink`) VALUES
  (1, 100, 'Ford', 'F-150', 2016, 'Black', 'Gasoline', 6, 'Automatic', 4, 'Truck', 1, 0, '1FUWDCXA8RH644710', 1, 'images/Trucks/fordF1502016Truck.jpg'),
  (2, 120, 'Nissan', 'Titan', 2016, 'Brown', 'Gasoline', 6, 'Automatic', 4, 'Truck', 1, 1, 'WBACG8321VKC71830', 1, 'images/Trucks/nissanTitan2016Truck.jpg'),
  (3, 100, 'Chevrolet', 'Silverado', 2016, 'Grey', 'Gasoline', 6, 'Automatic', 4, 'Truck', 0, 1, '1FMEU51KX8UA87042', 1, 'images/Trucks/chevroletSilverado2016Truck.jpg'),
  (4, 90, 'Ford', 'Explorer', 2016, 'Grey', 'Gasoline', 7, 'Automatic', 4, 'SUV', 0, 1, '1FMDU32X3VUC44710', 1, 'images/SUVs/fordExplorer2016Suv.jpg'),
  (5, 90, 'Honda', 'CRV', 2016, 'Grey', 'Gasoline', 5, 'Automatic', 4, 'SUV', 1, 0, '1XKDDB9X25R010375', 1, 'images/SUVs/hondaCRV2016Suv.jpg'),
  (6, 90, 'GMC', 'Acadia', 2016, 'Black', 'Gasoline', 8, 'Automatic', 4, 'SUV', 1, 1, '1FTPF12585NA87717', 1, 'images/SUVs/GMCAcadia2016Suv.jpg'),
  (7, 80, 'Toyota', 'Corolla', 2016, 'Red', 'Gasoline', 5, 'Automatic', 4, 'Car', 0, 1, '1G3WR54T6PD391823', 1, 'images/Cars/toyotaCorolla2016Car.jpg'),
  (8, 120, 'Cadillac', 'CTS', 2016, 'Grey', 'Gasoline', 5, 'Automatic', 4, 'Car', 1, 1, '1FTVX1EV9AKB57527', 1, 'images/Cars/cadillacCTS2016Car.jpg'),
  (9, 80, 'Honda', 'Civic', 2016, 'Black', 'Gasoline', 5, 'Automatic', 4, 'Car', 1, 1, '1FTDE14Y1JHA10838', 1, 'images/Cars/hondaCivic2016Car.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rentals`
--
ALTER TABLE `rentals`
ADD PRIMARY KEY (`id`), ADD KEY `vin` (`vin`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rentals`
--
ALTER TABLE `rentals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;

GRANT USAGE ON *.* TO 'rentcar'@'localhost' IDENTIFIED BY PASSWORD '*7AA76CF2BBFFCB9A848CFEE1902C83E340842A00';

GRANT ALL PRIVILEGES ON `rentacar`.* TO 'rentcar'@'localhost';