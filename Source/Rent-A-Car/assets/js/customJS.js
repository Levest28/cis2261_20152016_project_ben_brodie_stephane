/*
 * Author: Brodie MacBeath
 * Date: January 28, 2016
 * Updated: February 17, 2016
 * File: customJS.js
 * Purpose: This page is the custom JS file that is for all of the different forms and calls different verification
 * files and if there is an error returned then it will send the correct message back to the user.
 */

/**
 * This function is for the admin login page. When the admin tries to log in it submits through AJAX to the login
 * verification page and the login verification page returns certain strings depending on if there is no value entered
 * or if there is an error. Depending on what value is returned then there is an error or successful message being shown
 * to the user.
 *
 * @author Brodie MacBeath
 * @since January 28, 2016
 * @updated February 17, 2016
 */
$(document).ready(function() {
    //Submit messages
    var form = $('#frmAdminLogin'); // login form id
    var submit = $('#btnLoginSubmit');  // submit button id

    // form submit event
    form.on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '../verification/loginVerification.php', // url to send the post action to
            type: 'POST', // form submit method get/post
            data: form.serialize(), // serialize form data
            beforeSend: function() {
                $("#adminLoginError").html("Loading...")
            },
            success: function(data) {
                if (data == 'true')
                {
                    $(location).attr('href', '../index.php');
                }
                else
                {
                    $("#adminLoginError").html("* Wrong username or password *");
                }
            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});

/**
 * This function is for the contact us page. When the user enters information to send an email to the company it
 * submits the information through AJAX to the contact us verification page and the contact us page returns certain
 * strings depending on if there is no value entered or if there is an error. Depending on what value is returned
 * then there is an error or successful message being shown to the user.
 *
 * @author Brodie MacBeath
 * @since January 28, 2016
 * @updated February 17, 2016
 */
$(document).ready(function() {
    //Submit messages
    var form = $('#contactUsForm'); // login form id
    var submit = $('#btnContactSubmit');  // submit button id

    // form submit event
    form.on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: 'verification/contactUsVerification.php', // url to send the post action to
            type: 'POST', // form submit method get/post
            data: form.serialize(), // serialize form data
            beforeSend: function() {
                $("#contactUsError").html("Sending...")
            },
            success: function(data) {
                if (data == 'true')
                {
                    $("#contactUsError").html("* Your email has been sent. *");
                }
                else if (data == 'noFirstName')
                {
                    $("#contactUsError").html("* Please enter your first name. *");
                }
                else if (data == 'noLastName')
                {
                    $("#contactUsError").html("* Please enter your last name. *");
                }
                else if (data == 'noEmailAddress')
                {
                    $("#contactUsError").html("* Please enter your email address. *");
                }
                else if (data == 'noValidEmail')
                {
                    $("#contactUsError").html("* Please enter a valid email address. *");
                }
                else if (data == 'noPhoneNumber')
                {
                    $("#contactUsError").html("* Please enter your phone number. *");
                }
                else if (data == 'noMessage')
                {
                    $("#contactUsError").html("* Please enter a message. *");
                }
                else
                {
                    $("#contactUsError").html("* There was an error sending your email, please try again. *");
                }
            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});

/**
 * This function is for the admin vehicle rent page. When the admin enters the VIN to rent a vehicle it
 * submits the information through AJAX to the admin rent vehicle verification page and the admin rent vehicle
 * page returns certain strings depending on if there is no value entered or if there is an error.
 * Depending on what value is returned then there is an error or successful message being shown to the admin.
 *
 * @author Brodie MacBeath
 * @since January 28, 2016
 * @updated February 17, 2016
 */
$(document).ready(function() {
    //Submit messages
    var form = $('#frmAdminVehicleRent'); // login form id
    var submit = $('#btnRentVehicle');  // submit button id

    // form submit event
    form.on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '../verification/rentVehicleVerification.php', // url to send the post action to
            type: 'POST', // form submit method get/post
            data: form.serialize(), // serialize form data
            beforeSend: function() {
                $("#adminRentVehicleError").html("Renting...")
            },
            success: function(data) {
                if (data == 'true')
                {
                    $("#adminRentVehicleError").html("* The vehicle has been rented. *");
                }
                else if (data == 'noVIN')
                {
                    $("#adminRentVehicleError").html("* Please enter a VIN. *");
                }
                else if( data == 'noVehicle')
                {
                    $("#adminRentVehicleError").html("* There is no vehicle with that VIN. *");
                }
                else
                {
                    $("#adminRentVehicleError").html("* There was an error renting the vehicle, please try again. *");
                }
            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});

/**
 * This function is for the admin vehicle return page. When the admin enters the VIN to return a vehicle it
 * submits the information through AJAX to the admin return vehicle verification page and the admin return vehicle
 * page returns certain strings depending on if there is no value entered or if there is an error.
 * Depending on what value is returned then there is an error or successful message being shown to the admin.
 *
 * @author Brodie MacBeath
 * @since January 28, 2016
 * @updated February 17, 2016
 */
$(document).ready(function() {
    //Submit messages
    var form = $('#frmAdminVehicleReturn'); // login form id
    var submit = $('#btnReturnVehicle');  // submit button id

    // form submit event
    form.on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: '../verification/returnVehicleVerification.php', // url to send the post action to
            type: 'POST', // form submit method get/post
            data: form.serialize(), // serialize form data
            beforeSend: function() {
                $("#adminReturnVehicleError").html("Returning...")
            },
            success: function(data) {
                if (data == 'true')
                {
                    $("#adminReturnVehicleError").html("* The vehicle has been returned. *");
                    setTimeout( function(){ $(location).attr('href', '/admin/adminReturnVehicle.php'); } , 3000);
                }
                else if (data == 'noVIN')
                {
                    $("#adminReturnVehicleError").html("* Please enter a VIN. *");
                }
                else if( data == 'noVehicle')
                {
                    $("#adminReturnVehicleError").html("* There is no vehicle with that VIN. *");
                }
                else
                {
                    $("#adminReturnVehicleError").html("* There was an error returning the vehicle, please try again. *");
                }
            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});

/**
 * This function is for the book vehicle page. When the user tries to book a vehicle it submits through AJAX to the book
 * vehicle verification page and the book vehicle verification page returns certain strings depending on if there is
 * no value entered or if there is an error. Depending on what value is returned then there is an error or
 * successful message being shown to the user.
 *
 * @author Brodie MacBeath
 * @since January 28, 2016
 * @updated February 17, 2016
 */
$(document).ready(function() {
    //Submit messages
    var form = $('#frmBookVehicle'); // login form id
    var submit = $('#btnBookVehicle');  // submit button id

    // form submit event
    form.on('submit', function(e) {

        e.preventDefault(); // prevent default form submit

        $.ajax({
            url: 'verification/bookVehicleVerification.php', // url to send the post action to
            type: 'POST', // form submit method get/post
            data: form.serialize(), // serialize form data
            beforeSend: function() {
                $("#bookVehicleError").html("Loading...")
            },
            success: function(data) {
                if (data == 'true')
                {
                    $("#bookVehicleError").html("* Your vehicle has been successfully booked. *");
                    setTimeout( function(){ $(location).attr('href', 'index.php'); } , 3000);
                }
                else if (data == 'noFirstName')
                {
                    $("#bookVehicleError").html("* Please enter your first name. *");
                }
                else if (data == 'noLastName')
                {
                    $("#bookVehicleError").html("* Please enter your last name. *");
                }
                else if (data == 'noPhoneNumber')
                {
                    $("#bookVehicleError").html("* Please enter your phone number. *");
                }
                else if (data == 'phoneNumberNot10Numbers')
                {
                    $("#bookVehicleError").html("* Phone number must be 10 numbers. *");
                }
                else if (data == 'phoneNumberNotNumeric')
                {
                    $("#bookVehicleError").html("* Phone number must be numeric. *");
                }
                else if (data == 'noEmail')
                {
                    $("#bookVehicleError").html("* Please enter your email address. *");
                }
                else if (data == 'emailNotValid')
                {
                    $("#bookVehicleError").html("* Please enter a valid email address. *");
                }
                else if (data == 'noLicenseNumber')
                {
                    $("#bookVehicleError").html("* Please enter your license number. *");
                }
                else if (data == 'noLicenseOrigin')
                {
                    $("#bookVehicleError").html("* Please enter your license origin. *");
                }
                else if (data == 'noRentalPickUp')
                {
                    $("#bookVehicleError").html("* Please enter the rental pick up date. *");
                }
                else if (data == 'noRentalDropOff')
                {
                    $("#bookVehicleError").html("* Please enter the rental drop off date. *");
                }
                else if (data == 'pickUpDateInThePast')
                {
                    $("#bookVehicleError").html("* Please enter a date in the future for the rental pick up. *");
                }
                else if (data == 'dropOffDateInThePast')
                {
                    $("#bookVehicleError").html("* Please enter a date in the future for the rental drop off. *");
                }
                else if (data == 'dateInvalid')
                {
                    $("#bookVehicleError").html("* Please enter a valid date. *");
                }
                else if (data == 'dateTheSame')
                {
                    $("#bookVehicleError").html("* Please enter a valid date, rental pick up and drop off cannot be the same. *");
                }
                else
                {
                    $("#bookVehicleError").html("* There was an error booking your vehicle. Please try again later. *");
                }
            },
            error: function(e) {
                console.log(e)
            }
        });
    });
});