<?php
    /*
     * Author: Brodie MacBeath, Ben Bain, Stéphane Levesque
     * Date: January 15, 2016
     * Updated: February 17, 2016
     * File: index.php
     * Purpose: Main landing page for Rent-A-Car Rental Agency
     */

    require_once 'includes/includesCore.php';
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Rent-A-Car</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/customCSS.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body class="homepage">
    <div id="page-wrapper">

        <?php require('includes/includesHeader.php');?>

        <!-- Banner -->
        <div id="banner-wrapper">
            <div id="banner" class="box container">
                <div class="row">
                    <div class="6u 12u(medium)">
                        <h2>Welcome to Rent-A-Car!</h2>
                        <p>Rent-A-Car is a car rental agency that has all kinds of vehicles for you to choose from.</p>
                    </div>
                    <div class="6u 12u(medium)">
                        <ul>
                            <li><a href="/vehicles.php" class="button big icon fa-arrow-circle-right">Ok Let's Get Started!</a></li>
                            <li><a href="/about.php" class="button alt big icon fa-question-circle">More Company Info</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Features -->
        <section id="vehicles">
            <div id="features-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="4u 12u(medium)">

                            <!-- Box -->
                            <section class="box feature">
                                <a href="/vehicles.php?vehicles=trucks" class="image featured"><img src="images/Trucks/nissanTitan2016Truck.jpg"/></a>
                                <div class="inner">
                                    <header>
                                        <h2>Pick-Up Trucks</h2>
                                        <p>Got a heavy load? We can help.</p>
                                    </header>
                                    <p>We have a wide range of trucks that can be customized to your liking.</p>
                                </div>
                            </section>

                        </div>
                        <div class="4u 12u(medium)">

                            <!-- Box -->
                            <section class="box feature">
                                <a href="/vehicles.php?vehicles=suvs" class="image featured"><img src="images/SUVs/GMCAcadia2016Suv.jpg" alt="" /></a>
                                <div class="inner">
                                    <header>
                                        <h2>SUVs</h2>
                                        <p>Room for the whole family, and more.</p>
                                    </header>
                                    <p>We have a wide range of SUVs that can be customized to your liking.</p>
                                </div>
                            </section>

                        </div>
                        <div class="4u 12u(medium)">

                            <!-- Box -->
                            <section class="box feature">
                                <a href="/vehicles.php?vehicles=cars" class="image featured"><img src="images/Cars/hondaCivic2016Car.jpg" alt="" /></a>
                                <div class="inner">
                                    <header>
                                        <h2>Cars</h2>
                                        <p>Style and class, its your choice.</p>
                                    </header>
                                    <p>We have a wide range of cars that can be customized to your liking.</p>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <?php require("includes/includesFooter.php"); ?>

    </div>

    <!-- Scripts -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>

</body>
</html>